package com.jsp.grantservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jsp.grantservice.dto.GrantDto;
import com.jsp.grantservice.entity.GrantAssignment;
import com.jsp.grantservice.service.GrantService;

@RestController
@RequestMapping(value="/gs")
public class GrantServiceController {
	
	@Autowired
	private GrantService grantService;

	@PostMapping(value="/uploadGrant")
	public List<GrantAssignment> uploadGrant(@RequestBody List<GrantDto> list,@RequestHeader("key") String key) {
		List<GrantAssignment> listGrant = grantService.processUploadGrant(list);
		return listGrant;
	}
	
	@PostMapping(value="/approveOrAccept")
	public String approveOrAcceptGrant(@RequestParam List<Long> grantId,@RequestParam String operationType) {
		System.out.println(grantId);
		System.out.println(operationType);
		String string = grantService.processApproveOrAcceptGrant(grantId,operationType);
		return string;
	}
	
	@GetMapping(value="/pendingAllocationStatus/{allocationStatus}")
	public List<GrantAssignment> findGrantByAllocationStatus(@PathVariable("allocationStatus") String allocationStatus){
		List<GrantAssignment> list = grantService.processFindGrantByPendingAllocationStatus(allocationStatus);
		return list;
	}
	
}
