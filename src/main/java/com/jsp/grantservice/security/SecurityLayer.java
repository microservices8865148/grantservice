package com.jsp.grantservice.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

@Component
@WebFilter(urlPatterns = "*")
public class SecurityLayer implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
	
		HttpServletRequest httpServletRequest=(HttpServletRequest) request;
		String key="authorization";
		String header = httpServletRequest.getHeader("key");
		if(key.equals(header)) {
		chain.doFilter(httpServletRequest, response);
		}
		else throw new RuntimeException("authentication failure");
		
	}

}
