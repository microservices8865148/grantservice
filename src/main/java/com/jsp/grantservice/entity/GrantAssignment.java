package com.jsp.grantservice.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="grant_assignment")
public class GrantAssignment implements Serializable {

	@Id
	@Column(name="grant_id")
	@GenericGenerator(name="auto-reg", strategy="increment")
	@GeneratedValue(generator="auto-reg")
	private long grantId;
	
	@Column(name="employee_id")
	private long employeeId;
	
	@Column(name="band")
	private String band;
	
	@Column(name="number_of_grants")
	private int numberOfGrants;
	
	@Column(name="status")
	private String status;
	
	@Column(name="grant_price")
	private double grantPrice;
	
	@Column(name="accepted_date")
	private LocalDate acceptedDate;
	
	@Column(name="frequency")
	private int frequency;
	
	@Column(name="lock_in_status")
	private String lockInStatus;
	
	@Column(name="grant_date")
	private LocalDate grantDate;
	
	@Column(name="allocation_status")
	private String allocationStatus;

	public long getGrantId() {
		return grantId;
	}

	public void setGrantId(long grantId) {
		this.grantId = grantId;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public String getBand() {
		return band;
	}

	public void setBand(String band) {
		this.band = band;
	}

	public int getNumberOfGrants() {
		return numberOfGrants;
	}

	public void setNumberOfGrants(int numberOfGrants) {
		this.numberOfGrants = numberOfGrants;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getGrantPrice() {
		return grantPrice;
	}

	public void setGrantPrice(double grantPrice) {
		this.grantPrice = grantPrice;
	}

	public LocalDate getAcceptedDate() {
		return acceptedDate;
	}

	public void setAcceptedDate(LocalDate acceptedDate) {
		this.acceptedDate = acceptedDate;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public String getLockInStatus() {
		return lockInStatus;
	}

	public void setLockInStatus(String lockInStatus) {
		this.lockInStatus = lockInStatus;
	}

	public LocalDate getGrantDate() {
		return grantDate;
	}

	public void setGrantDate(LocalDate grantDate) {
		this.grantDate = grantDate;
	}

	public String getAllocationStatus() {
		return allocationStatus;
	}

	public void setAllocationStatus(String allocationStatus) {
		this.allocationStatus = allocationStatus;
	}
	
	
}
