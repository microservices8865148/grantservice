package com.jsp.grantservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jsp.grantservice.entity.GrantAssignment;

public interface GrantServiceRepository extends JpaRepository<GrantAssignment,Long> {

	
public List<GrantAssignment> findByAllocationStatus(String allocationStatus);
}
