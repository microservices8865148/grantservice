package com.jsp.grantservice.dto;

public class GrantDto {

	private long employeeId;
	
	private String band;
	
	private int numberOfGrants;
	
	private double grantPrice;
	
	private int frequency;

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public String getBand() {
		return band;
	}

	public void setBand(String band) {
		this.band = band;
	}

	public int getNumberOfGrants() {
		return numberOfGrants;
	}

	public void setNumberOfGrants(int numberOfGrants) {
		this.numberOfGrants = numberOfGrants;
	}

	public double getGrantPrice() {
		return grantPrice;
	}

	public void setGrantPrice(double grantPrice) {
		this.grantPrice = grantPrice;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	
	
}
