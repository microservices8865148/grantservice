package com.jsp.grantservice.service;

import java.util.List;

import com.jsp.grantservice.dto.GrantDto;
import com.jsp.grantservice.entity.GrantAssignment;

public interface GrantService {
	public List<GrantAssignment> processUploadGrant(List<GrantDto> list);
	
	public String processApproveOrAcceptGrant(List<Long> grantIdList,String generationType);
	
	public List<GrantAssignment> processFindGrantByPendingAllocationStatus(String allocationStatus);
	
	
}
