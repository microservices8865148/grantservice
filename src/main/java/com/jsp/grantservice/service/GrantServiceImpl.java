package com.jsp.grantservice.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.jsp.grantservice.dto.GrantDto;
import com.jsp.grantservice.entity.GrantAssignment;
import com.jsp.grantservice.repository.GrantServiceRepository;

@Service
public class GrantServiceImpl implements GrantService {
	
	@Autowired
	private GrantServiceRepository grantServiceRepository;
	
	@Autowired
	private KafkaTemplate<String,Object> kafkaTemplate;

 	@Override
	public List<GrantAssignment> processUploadGrant(List<GrantDto> list) {
		
		List<GrantAssignment> l = new ArrayList<GrantAssignment>();
	for (GrantDto grantDto : list) {
		GrantAssignment grantAssignment = new GrantAssignment();
		grantAssignment.setEmployeeId(grantDto.getEmployeeId());
		grantAssignment.setBand(grantDto.getBand());
		grantAssignment.setNumberOfGrants(grantDto.getNumberOfGrants());
		grantAssignment.setStatus("pending");
		grantAssignment.setGrantPrice(grantDto.getGrantPrice());
		grantAssignment.setFrequency(grantDto.getFrequency());
		grantAssignment.setAcceptedDate(null);
		grantAssignment.setLockInStatus("unlocked");
		grantAssignment.setGrantDate(null);
		grantAssignment.setAllocationStatus("pending");
		l.add(grantAssignment);
	}
	List<GrantAssignment> list2 = grantServiceRepository.saveAll(l);
		return list2;
	}
	
	@Override
	public String processApproveOrAcceptGrant(List<Long> grantIdList,String operationType) {

			List<GrantAssignment> list = grantServiceRepository.findAllById(grantIdList);
			System.out.println(list);
			for (GrantAssignment grantAssignment : list) {
				if(operationType.equalsIgnoreCase("approved")) {
				grantAssignment.setStatus(operationType);
				grantAssignment.setGrantDate(LocalDate.now());
				
				}
				else if(operationType.equalsIgnoreCase("accepted")) {
				grantAssignment.setStatus(operationType);
				grantAssignment.setAcceptedDate(LocalDate.now());
				
				}
				else {
					grantAssignment.setAllocationStatus(operationType);
					
				}
				
			}
			grantServiceRepository.saveAll(list);
			
		return new String("grant updated successfully");
	}

	@Override
	public List<GrantAssignment> processFindGrantByPendingAllocationStatus(String allocationStatus) {
	
		List<GrantAssignment> list = grantServiceRepository.findByAllocationStatus(allocationStatus);
		return list;
	}
	
	@Scheduled(fixedDelay=300000)
	public void sendData() {
		String s="hi good morning";
		List<GrantAssignment> list = processFindGrantByPendingAllocationStatus("pending");
		kafkaTemplate.send("list", list);
		
	}
	
}
